import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';
import { Cache } from 'cache-manager';
import axios from 'axios';
import { ProductInfo } from '../models/productinfo.interface';

@Injectable()
export class OpenfoodfactsService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  /**
   * This method will make an api call to OpenFoodFacts
   * @param barcode of the product
   * @returns the infos of the product 
   */
  async getProductInfo(barcode: string): Promise<ProductInfo> {
    const cachedData = await this.cacheManager.get<ProductInfo>(barcode);
    if (cachedData) {
      console.log(`Getting data from cache!` + cachedData);
        return cachedData;
    }

    const response = await axios.get(
        `https://world.openfoodfacts.org/api/v0/product/${barcode}.json`,
    );

    if (response.status !== 200) {
        throw new Error('Unable to retrieve product information');
    }

    const { data } = response;

    const productInfo: ProductInfo = {
        name: data.product.product_name,
        brand: data.product.brands,
        image: data.product.image_front_url,
        ingredients: data.product.ingredients_text,
    };
    await this.cacheManager.set(barcode, productInfo);
    
    console.log("Not in cache");
    return productInfo;
  }
}
 
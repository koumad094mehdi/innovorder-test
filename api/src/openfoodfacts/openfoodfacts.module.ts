import {CacheModule, Module } from '@nestjs/common';
import { OpenfoodfactsController } from './controller/openfoodfacts.controller';
import { OpenfoodfactsService } from './service/openfoodfacts.service';
import * as redisStore from 'cache-manager-ioredis';

@Module({
  imports: [
    CacheModule.registerAsync({
      useFactory: () => {
          return {
              store: redisStore,
              host: process.env.REDIS_HOST,
              port: process.env.REDIS_PORT,
              ttl: +process.env.REDIS_TTL
          }
      },
  }),
  ],
  providers: [OpenfoodfactsService],
  controllers: [OpenfoodfactsController]
})
export class OpenfoodfactsModule {}


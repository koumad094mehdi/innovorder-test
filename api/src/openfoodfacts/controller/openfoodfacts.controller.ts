import { Controller, Get, Param, Headers, HttpException, HttpStatus } from '@nestjs/common';
import { ProductInfo, ProductInfos } from '../models/productinfo.interface';
import { OpenfoodfactsService } from '../service/openfoodfacts.service';
import * as jwt from 'jsonwebtoken';
import { ApiBearerAuth, ApiHeader, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('openfoodfacts')
@ApiBearerAuth() 
@Controller('openfoodfacts')
export class OpenfoodfactsController {

    constructor(private readonly openfoodfactsService: OpenfoodfactsService) {}

    @ApiOperation({ summary: 'Get product information by barcode' })
    @ApiParam({ name: 'product', description: 'Barcode of the product', example: '1234567890123' })
    @ApiResponse({ status: 200, description: 'Product information', type: ProductInfos })
    @ApiResponse({ status: 401, description: 'Invalid token' })
    @Get(':product')
    productInfo(@Param() params, @Headers() headers): Promise<ProductInfo>{
        const token = headers.authorization.replace('Bearer ', '');
        try {
            const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
            return this.openfoodfactsService.getProductInfo(params.product);
        } catch (err) {
            throw new HttpException('Invalid Token', HttpStatus.UNAUTHORIZED);
        }
    }

}

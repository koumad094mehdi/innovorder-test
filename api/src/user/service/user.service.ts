import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, from, map, Observable, switchMap, throwError } from 'rxjs'; 
import { UserEntity } from '../models/user.entity';
import {User} from '../models/user.interface'
import { Repository } from 'typeorm';
import { AuthService } from 'src/auth/service/auth.service';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>,
        private authService: AuthService
    ){}
    
    /**
     * 
     * @param user 
     * @returns the user with its id
     */
    create(user: User): Observable<User> {
        return this.authService.hashPassword(user.password).pipe(
            switchMap((passwordHash: string) => {
                const newUser = new UserEntity();
                newUser.name = user.name;
                newUser.username = user.username;
                newUser.password = passwordHash;

                return from(this.userRepository.save(newUser)).pipe(
                    map((user: User) => {
                        const {password, ...result} = user;
                        return result;
                    }),
                    catchError(err => throwError(() => new Error(err)))
                )
            })
        )
    }

    /**
     * 
     * @param id of the user
     * @returns a User but we don't return the password
     */
    findOne(id: number): Observable<User>{
        return from(this.userRepository.findOne({ where: { id } })).pipe(
            map((user: User) => {
                const {password, ...result} = user;
                return result;
            })
        );
    }

    /**
     * 
     * @returns the list of users of the database
     */
    findAll(): Observable<User[]> {
        return from(this.userRepository.find()).pipe(
            map((users: User[]) => {
                users.forEach(function (v) {delete v.password});
                return users;
            })
        );
    }

    /**
     * 
     * @param id 
     * @param user We have a user in params, but we will not update username. It's a litte bit more complicated process
     * @returns 
     */
    updateOne(id: number, user: User): Observable<any> {
        delete user.username;
        
        return from(this.userRepository.update(id, user));
    }

    /**
     * 
     * @param username 
     * @returns a user (id, name, username) with the username given in parameters
     */
    findByUsername(username: string): Observable<User> {
        return from(this.userRepository.findOne({ where: { username } }));
    }

    /**
     * 
     * @param user 
     * @returns a JWT if the hash of the password of the user is the same as in the database
     */
    login(user: User): Observable<string> {
        return this.validateUser(user.username, user.password).pipe(
            switchMap((user: User) => {
                if(user) {
                    return this.authService.generateJWT(user).pipe(map((jwt: string) => jwt));
                } else {
                    console.log("Not the same");
                    return 'Wrong Credentials';
                }
            })
        )
    }

    /**
     * 
     * @param username 
     * @param password 
     * @returns a User (id, name, username)
     */
    validateUser(username: string, password: string): Observable<User> {
        return this.findByUsername(username).pipe(
            switchMap((user: User) => this.authService.comparePasswords(password, user.password).pipe(
                map((match: boolean) => {
                    if(match) {
                        const {password, ...result} = user;
                        return result;
                    } else {
                        throw Error;
                    }
                })
            ))
        )

    }

}

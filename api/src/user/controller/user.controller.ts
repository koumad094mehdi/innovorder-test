import { Body, Controller, Get, HttpException, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiParam, ApiResponse, ApiTags } from '@nestjs/swagger';
import { catchError, identity, map, Observable, of } from 'rxjs';
import { User, UserCreate, UserLogin, UserGet, UserUpdate} from '../models/user.interface';
import { UserService } from '../service/user.service';

@Controller('users')
@ApiTags('users')
export class UserController {

    constructor(private userService: UserService) {}

    @ApiOperation({ summary: 'Create a new user' })
    @ApiBody({ type: UserCreate })
    @ApiResponse({ status: 201, type: UserGet })
    @Post()
    create(@Body() user: User): Observable<User | Object> {
        return this.userService.create(user).pipe(
            map((user: User) => user),
            catchError(err => of({ error: err.message }))
        );
    }

    @ApiOperation({ summary: 'Log in a user' })
    @ApiBody({ type: UserLogin })
    @ApiResponse({ status: 200, description: 'Access Token', type: Object })
    @ApiResponse({ status: 401, description: 'Invalid Credential' })
    @Post('login')
    login(@Body() user: User): Observable<Object> {
        return this.userService.login(user).pipe(
            map((jwt: string) => {
                return { access_token: jwt };
            })
        )
    }

    @ApiOperation({ summary: 'Get a user by ID' })
    @ApiParam({ name: 'id', type: 'number' })
    @ApiResponse({ status: 200, type: UserGet })
    @ApiResponse({ status: 404, description: 'User Not Found' })
    @Get(':id')
    findOne(@Param() params): Observable<User>{
        try {
            const user = this.userService.findOne(params.id);
            return user;
        } catch (err) {
            throw new HttpException('User Not Found', HttpStatus.NOT_FOUND);
        }
        
    }

    @ApiOperation({ summary: 'Get all users' })
    @ApiResponse({ status: 200, type: [UserGet] })
    @Get()
    findAll(): Observable<User[]>{
        return this.userService.findAll();
    }

    @ApiOperation({ summary: 'Update a user by ID' })
    @ApiParam({ name: 'id', type: 'number' })
    @ApiBody({ type: UserUpdate })
    @ApiResponse({ status: 200 })
    @ApiResponse({ status: 404, description: 'User Not Found' })
    @Put(':id')
    updateOne(@Param('id') id: number, @Body() user: User): Observable<any>{
        try {
            const response = this.userService.updateOne(id, user);
            return response;
        } catch (err) {
            throw new HttpException('User Not Found', HttpStatus.NOT_FOUND);
        }
    }
}

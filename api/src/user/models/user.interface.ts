import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export interface User {    
    id?: number;
    username?: string;
    name?:string;
    password?:string;
}

export class UserCreate {
  
    @ApiProperty()
    username?: string;

    @ApiProperty()
    name?:string;
    
    @ApiProperty()
    password?:string;
}

export class UserLogin {
  
    @ApiProperty()
    username?: string;

    @ApiProperty()
    password?:string;
}

export class UserGet {
    
    @ApiProperty()
    id?:number;
    
    @ApiProperty()
    username?: string;

    @ApiProperty()
    name?:string;
    
}

export class UserUpdate {
    
    @ApiProperty()
    name?:string;
    
}


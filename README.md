# Projet Innovorder Test, Auteur: Mehdi KOUMAD

### Projet dans le cadre du processus de recrutement chez Innovorder

L'objectif de ce projet était de réaliser un CRUD et de requêter l'API OpenFoodFacts en NestJs. 

La langue utilisée dans le projet est l'anglais mise à part ce Readme.
<br>
<br>
## Présentation du Projet


Ce projet propose une API documentée par un swagger qui propose les fonctionnalités suivantes : 
1. Utilisateur 
- Création d'un utilisateur 
- Authentification d'un utilisateur (avec JWT)
- Obtention d'informations sur un utilisateur 
- Récupération de tous les utilisateurs 
- Modification des informations d'un utilisateur

2. OpenFoodFacts :
- Récupération de fiches produits à partir du code barre (sous condition d'authentification)

Les appels à l'API *OpenFoodFacts* sont mis en cache (Redis) pendant dix minutes.

Les utilisateurs sont stockés dans une base de données relationnelle (PostgreSQL)

L'application est conteneurisée avec Docker (docker compose).  

<br>

## Utilisation de l'application  
Pour lancer l'application : 

1. Clonez le repository
2. À la racine du projet, exécutez la commande "docker compose up" dans votre terminal
    
    
    NB : L'exécution de cette commande nécessite l'installation de Docker sur votre environnement.
    Pour plus d'informations sur l'installation de Docker, voir https://docs.docker.com/engine/install/
3. Pour accéder aux fonctionnalités disponibles : http://localhost:3000/swagger

<br>

*NB : Cette application ne contient pas de Manifest Kubernetes pour déploiement*
